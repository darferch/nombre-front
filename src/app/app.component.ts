import { Component } from '@angular/core';
import { SaludoService } from './saludo.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  saludo: string;
  loading = false;

  constructor(private service: SaludoService) { }

  enviar(nombre: string) {
    this.loading = true;
    this.service.saludar(nombre).pipe(
      finalize(() => this.loading = false)
    ).subscribe(x => this.saludo = x);

  }

}
